package com.bonial.kaufda.utils;

public final class Constants {

    private Constants() {
        // no instance
    }

    //Api Url
    public static final String BACKEND_API_URL = "http://prisma-sofa.rhcloud.com";
}
