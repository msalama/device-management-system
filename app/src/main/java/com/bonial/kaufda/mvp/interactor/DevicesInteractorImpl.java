package com.bonial.kaufda.mvp.interactor;

import android.support.annotation.NonNull;

import com.bonial.kaufda.data.api.DevicesServiceApi;
import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;
import com.bonial.kaufda.data.rest.DevicesRestClient;
import com.bonial.kaufda.mvp.OnFinishedListener;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.google.common.base.Preconditions.checkNotNull;

public class DevicesInteractorImpl implements DevicesInteractor {

    private DevicesServiceApi devicesServiceApi;

    public static DevicesInteractorImpl newInstance() {
        return new DevicesInteractorImpl();
    }

    public DevicesInteractorImpl() {
        devicesServiceApi = new DevicesRestClient().getService();
    }

    @Override
    public void getAllDevices(@NonNull final OnFinishedListener<List<Device>> listener) {

        checkNotNull(listener);

        devicesServiceApi.getAllDevices(new Callback<List<Device>>() {
            @Override
            public void success(List<Device> devices, Response response) {
                if (devices != null) {
                    listener.onSuccess(devices);
                } else {
                    listener.onFailure();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onFailure();
            }
        });
    }

    @Override
    public void getDeviceByNFC(@NonNull String nfcValue, @NonNull final OnFinishedListener<Device> listener) {

        checkNotNull(listener);

        devicesServiceApi.getDeviceByNFC(nfcValue, new Callback<Device>() {
            @Override
            public void success(Device device, Response response) {
                if (device != null) {
                    listener.onSuccess(device);
                } else {
                    listener.onFailure();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onFailure();
            }
        });
    }

    @Override
    public void updateDevice(@NonNull Device device, @NonNull final OnFinishedListener<DeviceUpdateResponse> listener) {
        checkNotNull(device);
        checkNotNull(listener);

        devicesServiceApi.updateDevice(device.getNfcvalue(), device, new Callback<DeviceUpdateResponse>() {
            @Override
            public void success(DeviceUpdateResponse deviceUpdateResponse, Response response) {
                listener.onSuccess(deviceUpdateResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onFailure();
            }
        });
    }
}
