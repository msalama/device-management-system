package com.bonial.kaufda.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.mvp.OnFinishedListener;
import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import com.bonial.kaufda.mvp.view.DevicesContract;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class DevicesPresenter implements DevicesContract.UserActionsListener {

    private DevicesContract.View devicesView;

    @NonNull
    private final DevicesInteractor devicesInteractor;

    public DevicesPresenter(@NonNull DevicesContract.View devicesView,
                            @NonNull DevicesInteractor devicesInteractor) {
        this.devicesView = checkNotNull(devicesView);
        this.devicesInteractor = checkNotNull(devicesInteractor);
    }


    @Override
    public void loadDevices() {

        devicesView.showProgress();

        devicesInteractor.getAllDevices(new OnFinishedListener<List<Device>>() {
            @Override
            public void onSuccess(@Nullable List<Device> devices) {

                if (devicesView == null) {
                    return;
                }

                devicesView.hideProgress();

                if (devices == null || devices.isEmpty()) {
                    devicesView.showEmptyDevices();
                } else {
                    devicesView.showDevices(devices);
                }
            }

            @Override
            public void onFailure() {

                if (devicesView == null) {
                    return;
                }

                devicesView.hideProgress();
                devicesView.showFailureMessage();
            }
        });
    }

    @Override
    public void openDeviceDetails(@NonNull String nfcValue) {
        checkNotNull(nfcValue);
        devicesView.showDeviceDetailsUi(nfcValue);
    }

    @Override
    public void stopPresenter() {
        devicesView = null;
    }
}
