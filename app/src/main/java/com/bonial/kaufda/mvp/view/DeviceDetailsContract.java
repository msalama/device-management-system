package com.bonial.kaufda.mvp.view;

import android.support.annotation.NonNull;

import com.bonial.kaufda.data.model.Device;

public interface DeviceDetailsContract {

    interface View {

        void showProgress();

        void hideProgress();

        void showDeviceName(String deviceName);

        void showBorrowerName(String borrowerName);

        void onDeviceLoaded(Device device);

        void onBorrowedDevice();

        void onEndDeviceLoan();

        void showDeviceFailureMessage();

        void showBorrowDeviceFailureMessage();

        void showEndDeviceLoanFailureMessage();

        void isAlreadyBorrowed(boolean isAlreadyBorrowed);
    }

    interface UserActionsListener {

        void loadDevice(String nfcValue);

        void endLoan(@NonNull Device device);

        void borrowDevice(@NonNull Device device, String borrowerName);

        void stopPresenter();
    }
}