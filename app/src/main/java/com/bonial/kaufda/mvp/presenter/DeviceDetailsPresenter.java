package com.bonial.kaufda.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;
import com.bonial.kaufda.mvp.OnFinishedListener;
import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import com.bonial.kaufda.mvp.view.DeviceDetailsContract;

import static com.google.common.base.Preconditions.checkNotNull;

public class DeviceDetailsPresenter implements DeviceDetailsContract.UserActionsListener {

    private DeviceDetailsContract.View deviceDetailsView;
    @NonNull
    private final DevicesInteractor devicesInteractor;

    public DeviceDetailsPresenter(@NonNull DeviceDetailsContract.View deviceDetailsView,
                                  @NonNull DevicesInteractor devicesInteractor) {
        this.deviceDetailsView = checkNotNull(deviceDetailsView);
        this.devicesInteractor = checkNotNull(devicesInteractor);
    }

    @Override
    public void loadDevice(String nfcValue) {
        deviceDetailsView.showProgress();
        devicesInteractor.getDeviceByNFC(nfcValue, new OnFinishedListener<Device>() {
            @Override
            public void onSuccess(@Nullable Device device) {

                if (deviceDetailsView == null) {
                    return;
                }

                deviceDetailsView.hideProgress();

                if (device == null) {
                    deviceDetailsView.showDeviceFailureMessage();
                    return;
                }

                deviceDetailsView.onDeviceLoaded(device);

                if (device.getHoldername() != null && !device.getHoldername().isEmpty()) {
                    deviceDetailsView.isAlreadyBorrowed(true);
                } else {
                    deviceDetailsView.isAlreadyBorrowed(false);
                }

                deviceDetailsView.showDeviceName(device.getDevicename());
                deviceDetailsView.showBorrowerName(device.getHoldername());
            }

            @Override
            public void onFailure() {

                if (deviceDetailsView == null) {
                    return;
                }

                deviceDetailsView.hideProgress();
                deviceDetailsView.showDeviceFailureMessage();
            }
        });
    }

    @Override
    public void endLoan(@NonNull Device device) {
        checkNotNull(device);

        device.setHoldername("");

        deviceDetailsView.showProgress();

        devicesInteractor.updateDevice(device, new OnFinishedListener<DeviceUpdateResponse>() {
            @Override
            public void onSuccess(@Nullable DeviceUpdateResponse deviceUpdateResponse) {

                if (deviceDetailsView == null) {
                    return;
                }

                deviceDetailsView.hideProgress();

                if (deviceUpdateResponse != null && deviceUpdateResponse.getNumberOfUpdates() > 0) {
                    deviceDetailsView.onEndDeviceLoan();
                } else {
                    deviceDetailsView.showEndDeviceLoanFailureMessage();
                }
            }

            @Override
            public void onFailure() {

                if (deviceDetailsView == null) {
                    return;
                }

                deviceDetailsView.hideProgress();
                deviceDetailsView.showEndDeviceLoanFailureMessage();
            }
        });
    }

    @Override
    public void borrowDevice(@NonNull Device device, String borrowerName) {
        checkNotNull(device);
        checkNotNull(borrowerName);

        device.setHoldername(borrowerName);

        deviceDetailsView.showProgress();

        devicesInteractor.updateDevice(device, new OnFinishedListener<DeviceUpdateResponse>() {
            @Override
            public void onSuccess(@Nullable DeviceUpdateResponse deviceUpdateResponse) {

                if (deviceDetailsView == null) {
                    return;
                }

                deviceDetailsView.hideProgress();

                if (deviceUpdateResponse != null && deviceUpdateResponse.getNumberOfUpdates() > 0) {
                    deviceDetailsView.onBorrowedDevice();
                } else {
                    deviceDetailsView.showBorrowDeviceFailureMessage();
                }
            }

            @Override
            public void onFailure() {

                if (deviceDetailsView == null) {
                    return;
                }

                deviceDetailsView.hideProgress();
                deviceDetailsView.showBorrowDeviceFailureMessage();
            }
        });
    }

    @Override
    public void stopPresenter() {
        deviceDetailsView = null;
    }
}
