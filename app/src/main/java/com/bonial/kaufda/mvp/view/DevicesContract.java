package com.bonial.kaufda.mvp.view;

import android.support.annotation.NonNull;


import com.bonial.kaufda.data.model.Device;

import java.util.List;

public interface DevicesContract {

    interface View {

        void showProgress();

        void hideProgress();

        void showDevices(List<Device> devices);

        void showEmptyDevices();

        void showFailureMessage();

        void showDeviceDetailsUi(String nfcValue);
    }

    interface UserActionsListener {

        void loadDevices();

        void openDeviceDetails(@NonNull String nfcValue);

        void stopPresenter();
    }
}
