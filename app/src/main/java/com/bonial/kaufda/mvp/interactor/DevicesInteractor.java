package com.bonial.kaufda.mvp.interactor;

import android.support.annotation.NonNull;

import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;
import com.bonial.kaufda.mvp.OnFinishedListener;

import java.util.List;

public interface DevicesInteractor {

    void getAllDevices(@NonNull OnFinishedListener<List<Device>> listener);

    void getDeviceByNFC(@NonNull String nfcValue, @NonNull OnFinishedListener<Device> listener);

    void updateDevice(@NonNull Device device, @NonNull OnFinishedListener<DeviceUpdateResponse> listener);
}
