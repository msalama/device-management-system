package com.bonial.kaufda.mvp;

import android.support.annotation.Nullable;

public interface OnFinishedListener<T> {

    void onSuccess(@Nullable T data);
    void onFailure();
}
