package com.bonial.kaufda.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bonial.kaufda.R;
import com.bonial.kaufda.adapter.DevicesAdapter;
import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.mvp.presenter.DevicesPresenter;
import com.bonial.kaufda.mvp.view.DevicesContract;
import com.bonial.kaufda.ui.activities.DeviceDetailsActivity;
import com.kaufda.de.dms.Injection;

import java.util.Collections;
import java.util.List;

public class DevicesListFragment extends Fragment implements DevicesContract.View {

    private ProgressBar progressBarLoading;

    private TextView emptyDevicesTextView;

    private DevicesAdapter devicesAdapter;

    private DevicesPresenter devicesPresenter;


    DevicesAdapter.DeviceItemListener deviceItemListener =
            new DevicesAdapter.DeviceItemListener() {

        @Override
        public void onDeviceClick(Device device) {
            devicesPresenter.openDeviceDetails(device.getNfcvalue());
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        devicesAdapter = new DevicesAdapter(Collections.<Device>emptyList(), deviceItemListener);

        devicesPresenter = new DevicesPresenter(
                this,
                Injection.getDevicesInteractorInstance()
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        devicesPresenter.loadDevices();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        devicesPresenter.stopPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_devices, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //init recycle view
        RecyclerView recyclerViewDevices =
                (RecyclerView) view.findViewById(R.id.devices_recycleView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewDevices.setLayoutManager(layoutManager);
        recyclerViewDevices.setHasFixedSize(true);
        recyclerViewDevices.setAdapter(devicesAdapter);

        progressBarLoading = (ProgressBar) view.findViewById(R.id.loading_progressBar);

        emptyDevicesTextView = (TextView) view.findViewById(R.id.textView_empty_offers);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void showProgress() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void showDevices(List<Device> devices) {
        devicesAdapter.replaceData(devices);
    }

    @Override
    public void showEmptyDevices() {
        emptyDevicesTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFailureMessage() {
        Snackbar.make(getView(),
                getString(R.string.error_failed_load_devices_message),
                Snackbar.LENGTH_SHORT).
                show();
    }

    @Override
    public void showDeviceDetailsUi(String nfcValue) {
        Uri.Builder builder = new Uri.Builder();
        builder.path(nfcValue);
        Uri uri = builder.build();

        Intent intent = new Intent(getActivity(), DeviceDetailsActivity.class);
        intent.setData(uri);
        startActivity(intent);
    }

}
