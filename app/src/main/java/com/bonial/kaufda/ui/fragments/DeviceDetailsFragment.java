package com.bonial.kaufda.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bonial.kaufda.R;
import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.mvp.presenter.DeviceDetailsPresenter;
import com.bonial.kaufda.mvp.view.DeviceDetailsContract;
import com.kaufda.de.dms.Injection;

public class DeviceDetailsFragment extends Fragment implements DeviceDetailsContract.View{


    private Device device;

    private String nfcValue;

    private DeviceDetailsPresenter deviceDetailsPresenter;

    private TextView deviceNameTextView;

    private View endLoanView;

    private TextView borrowerNameTextView;

    private View borrowDeviceView;

    private EditText borrowerNameEdt;

    private ProgressBar progressBarLoading;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Uri uri = getActivity().getIntent().getData();
        nfcValue = uri.getLastPathSegment();
        deviceDetailsPresenter = new DeviceDetailsPresenter(
                this,
                Injection.getDevicesInteractorInstance()
        );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        deviceNameTextView = (TextView) view.findViewById(R.id.deviceName_textView);

        endLoanView = view.findViewById(R.id.endLoan_view);

        borrowerNameTextView = (TextView) view.findViewById(R.id.borrowerName_textView);

        Button endLoanBtn = (Button) view.findViewById(R.id.endLoan_btn);
        endLoanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceDetailsPresenter.endLoan(device);
            }
        });

        borrowDeviceView = view.findViewById(R.id.borrowDevice_view);

        borrowerNameEdt = (EditText) view.findViewById(R.id.borrowerName_edt);

        Button borrowBtn = (Button) view.findViewById(R.id.borrow_btn);
        borrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String borrowerName = borrowerNameEdt.getText().toString();

                if (borrowerName.isEmpty()) {
                    borrowerNameEdt.setError(getString(R.string.error_empty_borrower_name));
                    return;
                }

                deviceDetailsPresenter.borrowDevice(device, borrowerName);
            }
        });

        progressBarLoading = (ProgressBar) view.findViewById(R.id.loading_progressBar);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        deviceDetailsPresenter.loadDevice(nfcValue);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deviceDetailsPresenter.stopPresenter();
    }

    @Override
    public void showProgress() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void showDeviceName(String deviceName) {
        deviceNameTextView.setText(deviceName);
    }

    @Override
    public void showBorrowerName(String borrowerName) {
        borrowerNameTextView.setText(borrowerName);
    }

    @Override
    public void onDeviceLoaded(Device device) {
        this.device = device;
    }

    @Override
    public void onBorrowedDevice() {
        deviceDetailsPresenter.loadDevice(device.getNfcvalue());
    }

    @Override
    public void onEndDeviceLoan() {
        deviceDetailsPresenter.loadDevice(device.getNfcvalue());
    }

    @Override
    public void showDeviceFailureMessage() {
        Snackbar.make(getView(),
                getString(R.string.error_failed_load_device_message),
                Snackbar.LENGTH_SHORT).
                show();
    }

    @Override
    public void showBorrowDeviceFailureMessage() {
        Snackbar.make(getView(),
                getString(R.string.error_failed_borrow_device_message),
                Snackbar.LENGTH_SHORT).
                show();
    }

    @Override
    public void showEndDeviceLoanFailureMessage() {
        Snackbar.make(getView(),
                getString(R.string.error_failed_end_loan_message),
                Snackbar.LENGTH_SHORT).
                show();
    }

    @Override
    public void isAlreadyBorrowed(boolean isAlreadyBorrowed) {
        if (isAlreadyBorrowed) {
            // enable end loan option
            borrowDeviceView.setVisibility(View.GONE);
            endLoanView.setVisibility(View.VISIBLE);
        } else {
            //enable borrow device option
            endLoanView.setVisibility(View.GONE);
            borrowDeviceView.setVisibility(View.VISIBLE);
        }
    }

}
