package com.bonial.kaufda.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.bonial.kaufda.R;

public class DevicesListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getString(R.string.devices_overview_activity_title));

        setContentView(R.layout.activity_list_devices);
    }

}
