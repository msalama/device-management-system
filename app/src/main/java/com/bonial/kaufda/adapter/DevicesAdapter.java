package com.bonial.kaufda.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bonial.kaufda.R;
import com.bonial.kaufda.data.model.Device;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.DeviceViewHolder> {

    private static final String TAG = "DevicesAdapter";

    private List<Device> devices;
    private DeviceItemListener deviceItemListener;

    public DevicesAdapter(List<Device> devices, DeviceItemListener deviceItemListener) {
        this.deviceItemListener = deviceItemListener;
        setList(devices);
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View deviceView = inflater.inflate(R.layout.device_item, parent, false);
        DeviceViewHolder holder = new DeviceViewHolder(deviceView, deviceItemListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder deviceViewHolder, int position) {
        Device device = devices.get(position);

        deviceViewHolder.textViewDeviceName.setText(
                device.getDevicename() != null? device.getDevicename() : ""
        );

        deviceViewHolder.textViewDeviceBorrower.setText(
                device.getHoldername() != null ? device.getHoldername() : ""
        );
    }

    public Device getItem(int position) {
        return devices.get(position);
    }

    public void replaceData(List<Device> devices) {
        setList(devices);
        notifyDataSetChanged();
    }

    private void setList(@NonNull List<Device> devices) {
        this.devices = checkNotNull(devices);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView textViewDeviceName;
        public TextView textViewDeviceBorrower;
        private DeviceItemListener deviceItemListener;


        public DeviceViewHolder(View itemView, DeviceItemListener deviceItemListener) {
            super(itemView);
            this.deviceItemListener = deviceItemListener;
            textViewDeviceName = (TextView) itemView.findViewById(R.id.deviceName_textView);
            textViewDeviceBorrower = (TextView) itemView.findViewById(R.id.deviceBorrower_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: ");
            int position = getAdapterPosition();
            Device device = getItem(position);
            deviceItemListener.onDeviceClick(device);
        }
    }

    public interface DeviceItemListener {

        void onDeviceClick(Device device);
    }
}
