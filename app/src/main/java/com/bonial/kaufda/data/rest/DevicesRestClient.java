package com.bonial.kaufda.data.rest;

import android.support.annotation.Nullable;


import com.bonial.kaufda.data.api.DevicesServiceApi;
import com.bonial.kaufda.utils.Constants;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

public class DevicesRestClient {

    private DevicesServiceApi devicesServiceApi;

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Chartset", "UTF-8");
            request.addHeader("Content-Type", "application/json");
        }
    };

    public DevicesRestClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.BACKEND_API_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        devicesServiceApi = restAdapter.create(DevicesServiceApi.class);
    }

    public DevicesServiceApi getService() {
        return devicesServiceApi;
    }

}
