
package com.bonial.kaufda.data.model;

import com.google.gson.annotations.SerializedName;

public class Device {


    @SerializedName("devicename")
    private String devicename;
    @SerializedName("holdername")
    private String holdername;
    @SerializedName("nfcvalue")
    private String nfcvalue;

    public Device(String devicename, String holdername, String nfcvalue) {
        this.devicename = devicename;
        this.holdername = holdername;
        this.nfcvalue = nfcvalue;
    }

    /**
     *
     * @return
     *     The devicename
     */
    public String getDevicename() {
        return devicename;
    }

    /**
     *
     * @param devicename
     *     The devicename
     */
    public void setDevicename(String devicename) {
        this.devicename = devicename;
    }

    /**
     *
     * @return
     *     The holdername
     */
    public String getHoldername() {
        return holdername;
    }

    /**
     *
     * @param holdername
     *     The holdername
     */
    public void setHoldername(String holdername) {
        this.holdername = holdername;
    }

    /**
     *
     * @return
     *     The nfcvalue
     */
    public String getNfcvalue() {
        return nfcvalue;
    }

    /**
     *
     * @param nfcvalue
     *     The nfcvalue
     */
    public void setNfcvalue(String nfcvalue) {
        this.nfcvalue = nfcvalue;
    }

}
