package com.bonial.kaufda.data.model;

import com.google.gson.annotations.SerializedName;

public class DeviceUpdateResponse {

    @SerializedName("ok")
    private Integer ok;
    @SerializedName("n")
    private Integer numberOfUpdates;

    public DeviceUpdateResponse(Integer ok, Integer numberOfUpdates) {
        this.ok = ok;
        this.numberOfUpdates = numberOfUpdates;
    }
    /**
     *
     * @return
     * The ok
     */
    public Integer getOk() {
        return ok;
    }

    /**
     *
     * @param ok
     * The ok
     */
    public void setOk(Integer ok) {
        this.ok = ok;
    }

    /**
     *
     * @return
     * The n
     */
    public Integer getNumberOfUpdates() {
        return numberOfUpdates;
    }

    /**
     *
     * @param n
     * The n
     */
    public void setNumberOfUpdates(Integer n) {
        this.numberOfUpdates = n;
    }

}