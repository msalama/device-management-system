package com.bonial.kaufda.data.api;

import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface DevicesServiceApi {

    @GET("/api/devices")
    void getAllDevices(Callback<List<Device>> devicesCallback);


    @GET("/api/devices/{nfc}")
    void getDeviceByNFC(@Path("nfc") String nfcValue, Callback<Device> callback);

    @PUT("/api/devices/{nfc}")
    void updateDevice(@Path("nfc") String nfcValue,
                      @Body Device device,
                      Callback<DeviceUpdateResponse> callback);

}
