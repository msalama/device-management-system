package com.kaufda.de.dms;


import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import com.bonial.kaufda.mvp.interactor.DevicesInteractorImpl;

public class Injection {

    private Injection() {
        // no instance
    }

    public synchronized static DevicesInteractor getDevicesInteractorInstance() {
        return DevicesInteractorImpl.newInstance();
    }
}
