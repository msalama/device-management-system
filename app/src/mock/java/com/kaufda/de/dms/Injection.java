package com.kaufda.de.dms;

import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import com.kaufda.de.dms.mvp.interactor.FakeDevicesInteractorImpl;

public class Injection {

    private Injection() {
        // no instance
    }

    public synchronized static DevicesInteractor getDevicesInteractorInstance() {
        return FakeDevicesInteractorImpl.newInstance();
    }
}
