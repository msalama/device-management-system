package com.kaufda.de.dms.mvp.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;
import com.bonial.kaufda.mvp.OnFinishedListener;
import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import java.util.List;

public class FakeDevicesInteractorImpl implements DevicesInteractor {
    private static DeviceUpdateResponse DEVICE_DETAILS_RESPONSE;
    private static Device DEVICE;
    private static List<Device> DEVICES;
    private static boolean IS_UPDATE_DEVICE_SUCCESS;
    private static boolean IS_GET_DEVICE_BY_NFC_SUCCESS;
    private static boolean IS_ALL_Devices_SUCCESS;

    public static FakeDevicesInteractorImpl newInstance() {
        return new FakeDevicesInteractorImpl();
    }

    @Override
    public void getAllDevices(@NonNull OnFinishedListener<List<Device>> listener) {
        if (IS_ALL_Devices_SUCCESS) {
            listener.onSuccess(DEVICES);
        } else {
            listener.onFailure();
        }
    }

    @Override
    public void getDeviceByNFC(@NonNull String nfcValue, @NonNull final OnFinishedListener<Device> listener) {
        if (IS_GET_DEVICE_BY_NFC_SUCCESS) {
            listener.onSuccess(DEVICE);
        } else {
            listener.onFailure();
        }
    }

    @Override
    public void updateDevice(@NonNull Device device, @NonNull final OnFinishedListener<DeviceUpdateResponse> listener) {
        if (IS_UPDATE_DEVICE_SUCCESS) {
            listener.onSuccess(DEVICE_DETAILS_RESPONSE);
        } else {
            listener.onFailure();
        }
    }

    @VisibleForTesting
    public static void setDeviceUpdateResponse(DeviceUpdateResponse deviceUpdateResponse) {
        DEVICE_DETAILS_RESPONSE = deviceUpdateResponse;
    }

    @VisibleForTesting
    public static void setDevice(Device device) {
        DEVICE = device;
    }

    @VisibleForTesting
    public static void setDevices(List<Device> devices) {
        DEVICES = devices;
    }

    @VisibleForTesting
    public static void setUpdateDeviceSuccess(boolean isUpdateDeviceSuccess) {
        IS_UPDATE_DEVICE_SUCCESS = isUpdateDeviceSuccess;
    }

    @VisibleForTesting
    public static void setGetDeviceByNFCSuccess(boolean isGetDeviceByNFCSuccess) {
        IS_GET_DEVICE_BY_NFC_SUCCESS = isGetDeviceByNFCSuccess;
    }

    @VisibleForTesting
    public static void setAllDevicesSuccess(boolean isAllDevicesSuccess) {
        IS_ALL_Devices_SUCCESS = isAllDevicesSuccess;
    }
}
