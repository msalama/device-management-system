package com.bonial.kaufda.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;
import android.test.suitebuilder.annotation.LargeTest;

import com.bonial.kaufda.R;
import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;
import com.kaufda.de.dms.mvp.interactor.FakeDevicesInteractorImpl;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.bonial.kaufda.custom.matcher.CustomMatcher.withError;
import static com.bonial.kaufda.custom.matcher.CustomMatcher.withToolbarTitle;
import static org.hamcrest.core.Is.is;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class DeviceDetailsActivityTest {

    private static String DEVICE_NAME = "aDeviceName";
    private static String HOLDER_NAME = "aHolderName";
    private static String NFC_VALUE = "aNFCValue";

    @Rule
    public ActivityTestRule<DeviceDetailsActivity> activityActivityTestRule =
            new ActivityTestRule<>(DeviceDetailsActivity.class, true, false);


    @Test
    public void openDeviceBorrowWithNFCIntent() {
        Intent intent = new Intent(NfcAdapter.ACTION_NDEF_DISCOVERED);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("prisma://borrow/opo"));
        getContext().startActivity(intent);
        matchToolbarTitle(getString(R.string.device_details_activity_title));
    }

    @Test
    public void showInputErrorWhenClickBorrowButtonAndEditNameIsEmpty() {
        Device device = new Device(DEVICE_NAME, "", NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        launchActivity();
        onView(withId(R.id.borrowerName_edt)).perform(clearText());
        onView(withId(R.id.borrow_btn)).perform(click());
        onView(withId(R.id.borrowerName_edt)).check(matches(
                withError(R.string.error_empty_borrower_name)
        ));
    }

    @Test
    public void showBorrowerNameAfterClickBorrowButton() {
        Device device = new Device(DEVICE_NAME, "", NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        FakeDevicesInteractorImpl.setUpdateDeviceSuccess(true);
        FakeDevicesInteractorImpl.setDeviceUpdateResponse(new DeviceUpdateResponse(1, 1));
        launchActivity();
        onView(withId(R.id.borrowerName_edt)).perform(typeText(HOLDER_NAME));
        onView(withId(R.id.borrow_btn)).perform(click());
        onView(withId(R.id.borrowerName_textView)).check(matches(withText(HOLDER_NAME)));
        onView(withId(R.id.loading_progressBar)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void showFailureErrorAfterClickBorrowButton() {
        Device device = new Device(DEVICE_NAME, "", NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        FakeDevicesInteractorImpl.setUpdateDeviceSuccess(false);
        FakeDevicesInteractorImpl.setDeviceUpdateResponse(new DeviceUpdateResponse(1, 1));
        launchActivity();
        onView(withId(R.id.borrowerName_edt)).perform(typeText(HOLDER_NAME));
        onView(withId(R.id.borrow_btn)).perform(click());
        onView(withText(R.string.error_failed_borrow_device_message)).
                check(matches(isDisplayed()));
        onView(withId(R.id.loading_progressBar)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void showBorrowerNameEditAfterClickEndLoanButton() {
        Device device = new Device(DEVICE_NAME, HOLDER_NAME, NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        FakeDevicesInteractorImpl.setUpdateDeviceSuccess(true);
        FakeDevicesInteractorImpl.setDeviceUpdateResponse(new DeviceUpdateResponse(1, 1));
        launchActivity();
        onView(withId(R.id.endLoan_btn)).perform(click());
        onView(withId(R.id.loading_progressBar)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.borrowerName_edt)).check(matches(isDisplayed()));
        onView(withId(R.id.borrow_btn)).check(matches(isDisplayed()));
    }

    @Test
    public void showFailureErrorAfterClickEndLoanButton() {
        Device device = new Device(DEVICE_NAME, HOLDER_NAME, NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        FakeDevicesInteractorImpl.setUpdateDeviceSuccess(false);
        FakeDevicesInteractorImpl.setDeviceUpdateResponse(new DeviceUpdateResponse(1, 1));
        launchActivity();
        onView(withId(R.id.endLoan_btn)).perform(click());
        onView(withText(R.string.error_failed_end_loan_message)).
                check(matches(isDisplayed()));
        onView(withId(R.id.loading_progressBar)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void showDeviceAttributesInUIViewWhenDeviceAlreadyBorrowed() {
        Device device = new Device(DEVICE_NAME, HOLDER_NAME, NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        launchActivity();
        onView(withId(R.id.deviceName_textView)).check(matches(withText(DEVICE_NAME)));
        onView(withId(R.id.borrowerName_textView)).check(matches(withText(HOLDER_NAME)));
        onView(withId(R.id.endLoan_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.borrow_btn)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.borrowerName_edt)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }


    @Test
    public void showDeviceAttributesInUIViewWhenDeviceAvailableForBorrowing() {
        Device device = new Device(DEVICE_NAME, "", NFC_VALUE);
        FakeDevicesInteractorImpl.setDevice(device);
        FakeDevicesInteractorImpl.setGetDeviceByNFCSuccess(true);
        launchActivity();
        onView(withId(R.id.deviceName_textView)).check(matches(withText(DEVICE_NAME)));
        onView(withId(R.id.borrowerName_textView)).check(matches(withText("")));
        onView(withId(R.id.borrow_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.borrowerName_edt)).check(matches(isDisplayed()));
        onView(withId(R.id.endLoan_btn)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }


    private static ViewInteraction matchToolbarTitle(CharSequence title) {
        return onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is(title))));
    }

    private String getString(int resId){
        return getInstrumentation().getTargetContext().getString(resId);
    }

    private void launchActivity() {
        Intent intent = new Intent();
        intent.setData(Uri.parse("prisma://borrow/opo"));
        activityActivityTestRule.launchActivity(intent);
    }

}