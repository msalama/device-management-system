package com.bonial.kaufda.ui.activities;

import android.content.Intent;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;
import android.test.suitebuilder.annotation.LargeTest;

import com.bonial.kaufda.R;
import com.bonial.kaufda.data.model.Device;
import com.google.common.collect.Lists;
import com.kaufda.de.dms.mvp.interactor.FakeDevicesInteractorImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.bonial.kaufda.custom.matcher.CustomMatcher.withItemText;
import static com.bonial.kaufda.custom.matcher.CustomMatcher.withToolbarTitle;
import static org.hamcrest.core.Is.is;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DevicesListActivityTest {

    private static String DEVICE_NAME = "aDeviceName";
    private static String HOLDER_NAME = "aHolderName";
    private static String NFC_VALUE = "aNFCValue";

    private static List<Device> DEVICES = Lists.newArrayList(new Device(DEVICE_NAME, HOLDER_NAME, NFC_VALUE));

    @Rule
    public ActivityTestRule<DevicesListActivity> activityActivityTestRule =
            new ActivityTestRule<>(DevicesListActivity.class, true, false);

    @Before
    public void setup() {
    }

    @Test
    public void showDevicesToListDevices() {
        FakeDevicesInteractorImpl.setDevices(DEVICES);
        FakeDevicesInteractorImpl.setAllDevicesSuccess(true);

        activityActivityTestRule.launchActivity(new Intent());
        onView(withId(R.id.devices_recycleView)).perform(scrollTo(hasDescendant(withText(DEVICE_NAME))));
        onView(withItemText(HOLDER_NAME)).check(matches(isDisplayed()));
        onView(withId(R.id.loading_progressBar)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void clickOnDeviceItemAndShowDeviceBorrowActivity() {
        FakeDevicesInteractorImpl.setDevices(DEVICES);
        FakeDevicesInteractorImpl.setAllDevicesSuccess(true);
        activityActivityTestRule.launchActivity(new Intent());
        onView(withId(R.id.devices_recycleView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        matchToolbarTitle(getString(R.string.device_details_activity_title));
    }

    private static ViewInteraction matchToolbarTitle(CharSequence title) {
        return onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is(title))));
    }

    private String getString(int resId){
        return getInstrumentation().getTargetContext().getString(resId);
    }
}