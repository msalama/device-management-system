package com.bonial.kaufda.mvp.presenter;


import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.data.model.DeviceUpdateResponse;
import com.bonial.kaufda.mvp.OnFinishedListener;
import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import com.bonial.kaufda.mvp.view.DeviceDetailsContract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DeviceDetailsPresenterTest {

    private static String DEVICE_NAME = "aDeviceName";
    private static String HOLDER_NAME = "aHolderName";
    private static String NFC_VALUE = "aNFCValue";

    private Device device = new Device(DEVICE_NAME, HOLDER_NAME, NFC_VALUE);
    private DeviceUpdateResponse deviceUpdateResponse = new DeviceUpdateResponse(1, 1);

    @Mock
    private DeviceDetailsContract.View deviceBorrowView;
    @Mock
    private DevicesInteractor devicesInteractor;

    private DeviceDetailsPresenter deviceDetailsPresenter;

    @Captor
    private ArgumentCaptor<OnFinishedListener<Device>> devicesCallbackCaptor;

    @Captor
    private ArgumentCaptor<OnFinishedListener<DeviceUpdateResponse>> deviceDetailsCallbackCaptor;


    @Before
    public void setUp() throws Exception {

        deviceDetailsPresenter = new DeviceDetailsPresenter(deviceBorrowView, devicesInteractor);
    }

    @Test
    public void loadDeviceFromBackendAndShowOnDeviceBorrowerUi() {

        device.setHoldername(null);

        deviceDetailsPresenter.loadDevice(NFC_VALUE);

        verify(deviceBorrowView).showProgress();

        verify(devicesInteractor).getDeviceByNFC(Mockito.any(String.class), devicesCallbackCaptor.capture());
        devicesCallbackCaptor.getValue().onSuccess(device);

        verify(deviceBorrowView).hideProgress();
        verify(deviceBorrowView).onDeviceLoaded(device);
        verify(deviceBorrowView).showDeviceName(device.getDevicename());
        verify(deviceBorrowView).showBorrowerName(device.getHoldername());
        verify(deviceBorrowView).isAlreadyBorrowed(false);
    }

    @Test
    public void loadDeviceFromBackendAndShowOnDetailUiWhenBorrowerIsNotNull() {

        device.setHoldername(HOLDER_NAME);

        deviceDetailsPresenter.loadDevice(NFC_VALUE);

        verify(deviceBorrowView).showProgress();

        verify(devicesInteractor).getDeviceByNFC(Mockito.any(String.class), devicesCallbackCaptor.capture());
        devicesCallbackCaptor.getValue().onSuccess(device);

        verify(deviceBorrowView).hideProgress();
        verify(deviceBorrowView).onDeviceLoaded(device);
        verify(deviceBorrowView).showDeviceName(device.getDevicename());
        verify(deviceBorrowView).showBorrowerName(device.getHoldername());
        verify(deviceBorrowView).isAlreadyBorrowed(true);
    }

    @Test
    public void borrowDeviceAndShowBorrowDeviceFailureMessage() {

        deviceUpdateResponse.setNumberOfUpdates(0);

        deviceDetailsPresenter.borrowDevice(device, HOLDER_NAME);

        verify(deviceBorrowView).showProgress();

        verify(devicesInteractor).updateDevice(Mockito.any(Device.class), deviceDetailsCallbackCaptor.capture());
        deviceDetailsCallbackCaptor.getValue().onSuccess(deviceUpdateResponse);

        verify(deviceBorrowView).hideProgress();
        verify(deviceBorrowView).showBorrowDeviceFailureMessage();
    }

    @Test
    public void borrowDeviceAndOnBorrowedDevice() {

        deviceUpdateResponse.setNumberOfUpdates(1);

        deviceDetailsPresenter.borrowDevice(device, HOLDER_NAME);

        verify(deviceBorrowView).showProgress();

        verify(devicesInteractor).updateDevice(Mockito.any(Device.class), deviceDetailsCallbackCaptor.capture());
        deviceDetailsCallbackCaptor.getValue().onSuccess(deviceUpdateResponse);

        verify(deviceBorrowView).hideProgress();
        verify(deviceBorrowView).onBorrowedDevice();
    }

    @Test
    public void endLoanAndOnEndDeviceLoan() {

        deviceUpdateResponse.setNumberOfUpdates(1);

        deviceDetailsPresenter.endLoan(device);

        verify(deviceBorrowView).showProgress();

        verify(devicesInteractor).updateDevice(Mockito.any(Device.class), deviceDetailsCallbackCaptor.capture());
        deviceDetailsCallbackCaptor.getValue().onSuccess(deviceUpdateResponse);

        verify(deviceBorrowView).hideProgress();
        verify(deviceBorrowView).onEndDeviceLoan();
    }


    @Test
    public void endLoanAndShowEndLoanDeviceFailureMessage() {

        deviceUpdateResponse.setNumberOfUpdates(0);

        deviceDetailsPresenter.endLoan(device);

        verify(deviceBorrowView).showProgress();

        verify(devicesInteractor).updateDevice(Mockito.any(Device.class), deviceDetailsCallbackCaptor.capture());
        deviceDetailsCallbackCaptor.getValue().onSuccess(deviceUpdateResponse);

        verify(deviceBorrowView).hideProgress();
        verify(deviceBorrowView).showEndDeviceLoanFailureMessage();
    }

}