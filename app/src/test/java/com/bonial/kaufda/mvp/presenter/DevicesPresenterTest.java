package com.bonial.kaufda.mvp.presenter;

import com.bonial.kaufda.data.model.Device;
import com.bonial.kaufda.mvp.OnFinishedListener;
import com.bonial.kaufda.mvp.interactor.DevicesInteractor;
import com.bonial.kaufda.mvp.view.DevicesContract;
import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DevicesPresenterTest {

    private static String DEVICE_NAME = "aDeviceName";
    private static String HOLDER_NAME = "aHolderName";
    private static String NFC_VALUE = "aNFCValue";

    private static List<Device> DEVICES = Lists.newArrayList(new Device(DEVICE_NAME, HOLDER_NAME, NFC_VALUE));

    @Mock
    private DevicesContract.View deviceView;

    @Mock
    private DevicesInteractor devicesInteractor;

    private DevicesPresenter devicesPresenter;

    @Captor
    private ArgumentCaptor<OnFinishedListener<List<Device>>> getAllDevicesCallbackCaptor;


    @Before
    public void setUp() throws Exception {
        devicesPresenter = new DevicesPresenter(deviceView, devicesInteractor);
        devicesPresenter.loadDevices();
    }

    @Test
    public void loadDevicesFromBackendAndLoadIntoView() {

        verify(deviceView).showProgress();

        verify(devicesInteractor).getAllDevices(getAllDevicesCallbackCaptor.capture());
        getAllDevicesCallbackCaptor.getValue().onSuccess(DEVICES);

        verify(deviceView).hideProgress();
        verify(deviceView).showDevices(DEVICES);
    }

    @Test
    public void loadDevicesFromBackendAndShowEmptyIfDevicesAreNull() {

        verify(deviceView).showProgress();

        verify(devicesInteractor).getAllDevices(getAllDevicesCallbackCaptor.capture());
        getAllDevicesCallbackCaptor.getValue().onSuccess(null);

        verify(deviceView).hideProgress();
        verify(deviceView).showEmptyDevices();
    }

    @Test
    public void loadDevicesFromBackendAndShowEmptyIfDevicesAreEmpty() {

        verify(deviceView).showProgress();

        verify(devicesInteractor).getAllDevices(getAllDevicesCallbackCaptor.capture());
        getAllDevicesCallbackCaptor.getValue().onSuccess(Collections.<Device>emptyList());

        verify(deviceView).hideProgress();
        verify(deviceView).showEmptyDevices();
    }

    @Test
    public void loadDevicesFromBackendAndShowFailureMessage() {

        verify(deviceView).showProgress();

        verify(devicesInteractor).getAllDevices(getAllDevicesCallbackCaptor.capture());
        getAllDevicesCallbackCaptor.getValue().onFailure();

        verify(deviceView).hideProgress();
        verify(deviceView).showFailureMessage();
    }

    @Test
    public void clickOnDeviceShowsDetailUi() {
        devicesPresenter.openDeviceDetails(NFC_VALUE);
        verify(deviceView).showDeviceDetailsUi(NFC_VALUE);
    }


}